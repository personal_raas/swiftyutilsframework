//
//  Service.swift
//  SwiftyUtilsFramework
//
//  Created by Rashid Abbas on 15/11/2018.
//  Copyright © 2018 Rashid Abbas. All rights reserved.
//

import Foundation

public class Service {
    private init() {}
    public static func doSomething() {
        print("Hello Swifty Utils Framework...")
    }
}
